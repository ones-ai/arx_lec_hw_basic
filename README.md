# RISC-V eXpress #

## Overview ##

* RISC-V eXpress (RVX) is the HW/SW development kit for designing MCU-level low power SoCs.
* Kyuseung Han (han@etri.re.kr), ETRI, Republic of Korea.
* https://ieeexplore.ieee.org/document/9208740

## RVX Mini Install ##

* RVX Mini is the minimal version of RVX, designed to operate in conjunction with the RVX server rather than as a standalone system.
* After obtaining an account of RVX server, follow the installation manual.
* ./rvx_install/rvx_install_manual.pdf
